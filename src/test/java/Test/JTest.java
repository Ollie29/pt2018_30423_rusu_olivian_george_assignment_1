package Test;

import Model.Monom;
import Model.Polinom;
import Operation.Operation;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;

import static org.junit.jupiter.api.Assertions.*;

public class JTest {

    @Test
    public void testAddition(){
        Polinom p1 = new Polinom();
        Polinom p2 = new Polinom();
        Polinom result = new Polinom();
        Polinom preresult = new Polinom();

        p1.addMonom(new Monom(-4,2));
        p1.addMonom(new Monom(3,1));
        p1.addMonom(new Monom(1,0));

        p2.addMonom(new Monom(4,2));
        p2.addMonom(new Monom(3,1));
        p2.addMonom(new Monom(-5,3));
        p2.addMonom(new Monom(2,5));

        result.addMonom(new Monom(6,1));
        result.addMonom(new Monom(1,0));
        result.addMonom(new Monom(-5,3));
        result.addMonom(new Monom(2,5));

        preresult = Operation.addition(p1,p2);
        Assertions.assertEquals(preresult.getTerms().toString(), result.getTerms().toString());
    }

    @Test
    public void testSubtraction(){
        Polinom p1 = new Polinom();
        Polinom p2 = new Polinom();
        Polinom result = new Polinom();
        Polinom preresult = new Polinom();

        p1.addMonom(new Monom(-4,2));
        p1.addMonom(new Monom(3,1));
        p1.addMonom(new Monom(1,0));

        p2.addMonom(new Monom(4,2));
        p2.addMonom(new Monom(3,1));
        p2.addMonom(new Monom(-5,3));
        p2.addMonom(new Monom(2,5));

        result.addMonom(new Monom(-8,2));
        result.addMonom(new Monom(1,0));
        result.addMonom(new Monom(5,3));
        result.addMonom(new Monom(-2,5));

        preresult = Operation.subtraction(p1,p2);
        Assertions.assertEquals(preresult.getTerms().toString(), result.getTerms().toString());
    }

    @Test
    public void testMultiplication(){
        Polinom p1 = new Polinom();
        Polinom p2 = new Polinom();
        Polinom result = new Polinom();
        Polinom preresult = new Polinom();

        p1.addMonom(new Monom(-4,2));
        p1.addMonom(new Monom(3,1));
        p1.addMonom(new Monom(1,0));

        p2.addMonom(new Monom(4,2));
        p2.addMonom(new Monom(3,1));
        p2.addMonom(new Monom(-5,3));
        p2.addMonom(new Monom(2,5));

        result.addMonom(new Monom(-31,4));
        result.addMonom(new Monom(22,5));
        result.addMonom(new Monom(-8,7));
        result.addMonom(new Monom(13,2));
        result.addMonom(new Monom(6,6));
        result.addMonom(new Monom(3,1));
        result.addMonom(new Monom(-5,3));


        preresult = Operation.multiplication(p1,p2);
        Assertions.assertEquals(preresult.getTerms().toString(), result.getTerms().toString());
    }

    @Test
    public void testDerivation(){
        Polinom p1 = new Polinom();
        Polinom preresult = new Polinom();
        Polinom result = new Polinom();

        p1.addMonom(new Monom(-4,2));
        p1.addMonom(new Monom(3,1));
        p1.addMonom(new Monom(1,0));

        result.addMonom(new Monom(-8,1));
        result.addMonom(new Monom(3,0));

        preresult = Operation.derivation(p1);
        Assertions.assertEquals(preresult.getTerms().toString(), result.getTerms().toString());
    }

    @Test
    public void testIntegration(){
        Polinom p1 = new Polinom();
        Polinom preresult = new Polinom();
        Polinom result = new Polinom();

        p1.addMonom(new Monom(-4,2));
        p1.addMonom(new Monom(3,1));
        p1.addMonom(new Monom(1,0));

        result.addMonom(new Monom(-1.33333333333,3));
        result.addMonom(new Monom(1.5,2));
        result.addMonom(new Monom(1,1));

        preresult = Operation.integration(p1);
        Assertions.assertEquals(preresult.getTerms().toString(), result.getTerms().toString());
    }

}