package Operation;

import Model.Monom;
import Model.Polinom;

public class Operation {

    public static Polinom addition(Polinom p1, Polinom p2) {
        for (Monom m2 : p2.getTerms()) {
            p1.addMonom(m2);
        }
        return p1;
    }

    public static Polinom subtraction(Polinom p1, Polinom p2){
        for(Monom m2 : p2.getTerms()){
            m2.setCoeff(-(m2.getCoeff()));
            p1.addMonom(m2);
        }
        return p1;
    }

    public static Polinom multiplication(Polinom p1, Polinom p2){
        Polinom result = new Polinom();
        for(Monom m1 : p1.getTerms()){
            for(Monom m2 : p2.getTerms()){
                result.addMonom(new Monom(m1.getCoeff()*m2.getCoeff(), m1.getDegree()+m2.getDegree()));
            }
        }
        return result;
    }

    public static Polinom derivation(Polinom p1){
        Polinom result = new Polinom();
        for(Monom m : p1.getTerms()){
            result.addMonom(new Monom(m.getCoeff()*m.getDegree(),m.getDegree()-1));
        }
        return result;
    }

    public static Polinom integration(Polinom p1){
        Polinom result = new Polinom();
        for(Monom m : p1.getTerms()){
            result.addMonom(new Monom(m.getCoeff()/(m.getDegree()+1),m.getDegree()+1));
        }
        return result;
    }

}