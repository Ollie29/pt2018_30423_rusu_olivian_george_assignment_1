package Model;

import java.util.ArrayList;
import java.util.List;

public class Polinom {

    private List<Monom> polinom;

    public Polinom() {
        polinom = new ArrayList<Monom>();
    }

    public void addMonom(Monom m) {
        boolean check = false;
        for (Monom p : polinom) {
            if (p.getDegree() == m.getDegree()) {
                p.add(m);
                if(p.getCoeff() == 0)
                    polinom.remove(polinom.indexOf(p));
                check = true;
                break;
            }
        }
        if (!check) {
            if(m.getCoeff()!=0) {
                polinom.add(m);
            }
        }
    }

    public void copyPolinom(Polinom aux){
        for(Monom m : polinom){
            aux.addMonom(m);
        }
    }

    public List<Monom> getTerms(){
        return polinom;
    }

    @Override
    public String toString() {
        String empty="";
        for(Monom m : polinom){
            empty+=m.toString();
        }
        return empty;
    }
}

