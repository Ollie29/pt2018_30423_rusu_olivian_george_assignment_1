package Model;

import java.util.Comparator;

public class MonomComparator implements Comparator<Monom> {

    public int compare(Monom monom, Monom t1) {
        return (int) (t1.getDegree()-monom.getDegree());
    }
}
