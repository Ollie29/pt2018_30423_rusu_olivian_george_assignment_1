package Model;

import java.text.DecimalFormat;
import java.text.NumberFormat;

public class Monom {

    private double coeff;
    private double degree;
    private NumberFormat nf = new DecimalFormat("###.###");

    public Monom(double coeff, double degree) {
        this.coeff = coeff;
        this.degree = degree;
    }

    public void add(Monom m){
        if(this.degree == m.degree){
            this.coeff += m.coeff;
        }
    }

    public double getCoeff() {
        return coeff;
    }

    public double getDegree() {
        return degree;
    }

    public void setCoeff(double coeff) {
        this.coeff = coeff;
    }

    @Override
    public String toString() {
        if(this.coeff>0)
            return "+"+nf.format(this.coeff)+"X^"+nf.format(this.degree);
        else
            return nf.format(this.coeff)+"X^"+nf.format(this.degree);
    }
}
