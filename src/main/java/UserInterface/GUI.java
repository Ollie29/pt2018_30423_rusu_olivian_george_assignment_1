package UserInterface;

import Model.Monom;
import Model.MonomComparator;
import Model.Polinom;
import Operation.Operation;
import Utility.ParsePolinom;
import sun.java2d.windows.GDIRenderer;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collections;
import java.util.Comparator;

public class GUI {

    public static void main(String[] args) {

        JFrame frame = new JFrame("Polynomial Calculator");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(800,200);

        JPanel input1 = new JPanel();
        input1.setLayout(new FlowLayout());
        JPanel input2 = new JPanel();
        input2.setLayout(new FlowLayout());
        JPanel buttons = new JPanel();
        buttons.setLayout(new FlowLayout());
        JPanel display = new JPanel();
        display.setLayout(new FlowLayout());
        JPanel finale = new JPanel();
        finale.setLayout(new BoxLayout(finale, BoxLayout.Y_AXIS));

        JLabel label1 = new JLabel("Polinom 1 ");
        JLabel label2 = new JLabel("Polinom 2 ");
        final JTextField entry1 = new JTextField("Introdu Polinom 1",20);
        final JTextField entry2 = new JTextField("Introdu Polinom 2",20);
        JButton addButton = new JButton("Addition");
        JButton subButton = new JButton("Subtraction");
        JButton mulButton = new JButton("Multiplication");
        JButton derButton = new JButton("Derivation of Polinom 1");
        JButton intButton = new JButton("Integration of Polinom 1");
        final JTextArea displayArea = new JTextArea();
        JLabel label3 = new JLabel("Rezultat ");

        input1.add(label1);
        input1.add(entry1);
        input2.add(label2);
        input2.add(entry2);
        buttons.add(addButton);
        buttons.add(subButton);
        buttons.add(mulButton);
        buttons.add(derButton);
        buttons.add(intButton);
        display.add(label3);
        display.add(displayArea);

       addButton.addActionListener(new ActionListener() {
           public void actionPerformed(ActionEvent actionEvent) {
               String text1 = entry1.getText();
               String text2 = entry2.getText();

               Polinom polinom1 = ParsePolinom.parsingPolinom(text1);
               Polinom polinom2 = ParsePolinom.parsingPolinom(text2);

               Polinom result = Operation.addition(polinom1,polinom2);
               Collections.sort(result.getTerms(), new MonomComparator());
               displayArea.setText(result.toString());
               displayArea.update(displayArea.getGraphics());
           }
       });

       subButton.addActionListener(new ActionListener() {
           public void actionPerformed(ActionEvent actionEvent) {
               String text1 = entry1.getText();
               String text2 = entry2.getText();

               Polinom polinom1 = ParsePolinom.parsingPolinom(text1);
               Polinom polinom2 = ParsePolinom.parsingPolinom(text2);

               Polinom result = Operation.subtraction(polinom1,polinom2);
               Collections.sort(result.getTerms(), new MonomComparator());
               displayArea.setText(result.toString());
               displayArea.update(displayArea.getGraphics());

           }
       });

       mulButton.addActionListener(new ActionListener() {
           public void actionPerformed(ActionEvent actionEvent) {
               String text1 = entry1.getText();
               String text2 = entry2.getText();

               Polinom polinom1 = ParsePolinom.parsingPolinom(text1);
               Polinom polinom2 = ParsePolinom.parsingPolinom(text2);

               Polinom result = Operation.multiplication(polinom1,polinom2);
               Collections.sort(result.getTerms(), new MonomComparator());
               displayArea.setText(result.toString());
               displayArea.update(displayArea.getGraphics());
           }
       });

       derButton.addActionListener(new ActionListener() {
           public void actionPerformed(ActionEvent actionEvent) {
               String text1 = entry1.getText();

               Polinom polinom1 = ParsePolinom.parsingPolinom(text1);

               Polinom result = Operation.derivation(polinom1);
               Collections.sort(result.getTerms(), new MonomComparator());
               displayArea.setText(result.toString());
               displayArea.update(displayArea.getGraphics());
           }
       });

       intButton.addActionListener(new ActionListener() {
           public void actionPerformed(ActionEvent actionEvent) {
               String text1 = entry1.getText();

               Polinom polinom1 = ParsePolinom.parsingPolinom(text1);

               Polinom result = Operation.integration(polinom1);
               Collections.sort(result.getTerms(), new MonomComparator());
               displayArea.setText(result.toString());
               displayArea.update(displayArea.getGraphics());
           }
       });

        finale.add(input1);
        finale.add(input2);
        finale.add(buttons);
        finale.add(display);
        //finale.add(Box.createVerticalStrut(100));
        frame.setContentPane(finale);
        frame.setVisible(true);
    }
}
