package Utility;

import Model.Monom;
import Model.Polinom;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ParsePolinom {

    public static Polinom parsingPolinom(String polinom){

            Polinom result = new Polinom();
            String[] polinomSplit = polinom.split("[-|+]");

            for(int i=0;i<polinomSplit.length;i++){
                String monom = polinomSplit[i];
                String semn = "+";
                if(i>0) {
                    semn = polinom.substring(polinom.indexOf(polinomSplit[i]) - 1, polinom.indexOf(polinomSplit[i]));
                }
                String[] monomSplit = monom.split("[X|x^]"); // index 0 hold coeff, index 1 -> space, index 2 -> deg;
                if(monomSplit[0].length() > 0) { // condition put because first element of polinomSplit is 0length when first sign added
                    Integer coef = Integer.parseInt(semn+monomSplit[0]);
                    Integer deg = Integer.parseInt(monomSplit[2]);
                    Monom m = new Monom(coef, deg);
                    result.addMonom(m);
                }
            }
        return result;
    }
}
